'use strict';
module.exports = {
    entry: "./entry",
    output: {
        path: __dirname + "/dist",
        filename: "build.js"
    }
};