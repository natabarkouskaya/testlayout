(function(){
    'use strict';
    
    var headerElement = document.getElementsByTagName('header')[0],
        navMenu = document.getElementsByClassName('nav-menu')[0],
        liArray = navMenu.getElementsByTagName('li'),
        subMenu = navMenu.getElementsByClassName('nav-menu_hidden'),
        searchElement = document.getElementsByClassName('js-search')[0],
        searchForm = searchElement.firstElementChild,
        searchBtn = document.getElementById('btn-reset');
    
    function openNavigation(event){
        var target = event.target || event.srcElement,
            topValue = navMenu.clientHeight;
        if(target === liArray[0] || target === liArray[0].getElementsByTagName('span')[0] || target === liArray[0].getElementsByClassName('li_triangle')[0]){
            subMenu[0].classList.toggle('nav-menu_open');
        }
    }
    
    function closeNavigation(event){
        var target = event.target || event.srcElement;
        if(target !== liArray[0] && target !== liArray[0].firstElementChild){
            subMenu[0].classList.remove('nav-menu_open');
        }
        if(target !== searchElement){
            searchForm.classList.remove('search_open');
            searchForm.reset();
        }
    }
    
    function showSearch(e){
        e.stopImmediatePropagation();
        e.stopPropagation();
        var target = event.target || event.srcElement;
        searchForm.classList.toggle('search_open');
        if(target !== searchElement){
            searchForm.classList.add('search_open');
        }   
    }
    
    function resetForm(event){
        searchForm.reset(); 
    }
    
    document.addEventListener('click',closeNavigation, false);
    navMenu.addEventListener('click',openNavigation, false);
    searchElement.addEventListener('click',showSearch,false);
    searchBtn.addEventListener('click',resetForm,false);
    searchBtn.removeEventListener('click',showSearch,false);
})();